package main

import (
	"flag"
	"fmt"
	"log"
	"os"
	"sync"

	"gopl.io/ch5/links"
)

//!+sema
// type DeepSearcher interface {
// 	crawl(string, int)
// 	extract(string) []string
// }

type SafeSeen struct {
	seen     map[string]bool
	mux      sync.Mutex
	nodes    []string
	maxDepth int
	wg       sync.WaitGroup
	tokens   chan struct{}
}

func (s *SafeSeen) extract(u string) []string {
	// limit url requests, block if  >20 requests are active
	s.tokens <- struct{}{} // acquire a token
	l, err := links.Extract(u)
	<-s.tokens // release the token
	if err != nil {
		log.Print(err)
	}
	return l
}

func (s *SafeSeen) crawl(url string, depth int) {
	defer s.wg.Done()
	fmt.Printf("%s at depth %v\n", url, depth)

	// 1st commandment
	if depth >= s.maxDepth {
		return
	}

	for _, link := range s.extract(url) {
		s.mux.Lock()      // acquire link exclusively
		if s.seen[link] { // if seen skip
			s.mux.Unlock()
			continue
		}
		s.seen[link] = true // mark now as seen
		s.mux.Unlock()      // freeing this link is now safe as it will be skipped

		// recursively call - reduce condition
		s.wg.Add(1)
		go s.crawl(link, depth+1)
	}
}

//!-sema

//!+
func main() {
	s := &SafeSeen{
		seen:     make(map[string]bool),
		mux:      sync.Mutex{},
		nodes:    []string{}, // target urls for the DeepSearcher
		maxDepth: 2,
		wg:       sync.WaitGroup{},
		tokens:   make(chan struct{}, 20), // counting semaphore: enforce a limit of 20 concurrent HTTP GET requests.
	}

	if len(os.Args) == 1 {
		log.Fatalf("USAGE: crawl3 -d %d https://gopl.io ...", s.maxDepth)
	}

	flag.IntVar(&s.maxDepth, "d", 2, "set max depth of crawl")
	flag.Parse()
	s.nodes = flag.Args()

	for _, link := range s.nodes {
		s.wg.Add(1)
		go s.crawl(link, 0)
	}
	s.wg.Wait()
}

//!-~
